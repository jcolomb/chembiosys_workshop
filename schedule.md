| Time        | Who           | What  |
| ------------- |:-------------:| -----:|
|9:00- 9:30    |all   | Welcome address and getting to know each other |
|9:30 - 10:30  |(Chris, or Julien, or Maria) | Data management and sharing: The big picture (open science, open data, FAIR data: the why, the what, the how and the where). Metabolomics the story (Chris, 15-20min). |
|10:30 |*Coffee break*| |
|11:00- 11:45 | Julien|Data acquisition, principles: data types, data organisation, human and computer readability interoperability and standards |
|11h45 -12:30| Maria |  Data acquisition: ISA tools|
|12:30| *Lunch*| |
|13:30- 14:15| Julien chris| The data “life cycle” : recap, data flow, data storage, data sharing, data management plan, questions|
|14:15 - 15:00| Maria Julien| Upload your data: where and how (difference zenodo/seek, practical exercise)|
|15:00| *Coffee break*| |
|15:30- 16:15| Maria Julien| Advanced SEEK, Reproducibility, SOPs, Protocols, etc...|
|16:15-17:00| all | Summary and feedback  |
---

17:00 End of workshop

Place:

Seminarroom 127, Lessingstr. 8, 07743 Jena
(chembiosys offices : second floor, room 228)

