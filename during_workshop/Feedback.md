# Feedbacks
# final feedback

- the good
    - small size
    - time for questions/discussions
    - relaxed atmosphere
    - new tools introduced
    - free coffee and food, finished on time
    - convinced it is not an administrative task.
- To ameliorate
    - need more time, especially for exercices
    - need more explanation on why we do this
    - not convince that it saves time
    - little whoaa effects, could be more exciting
    - some questions would need more explanations (especiall when non-native speakers are present)
    
## videos:

- need to make more prominent that researcher have to go open data

## ontologies

- most have heard of it
- 1/7 know where to look for it

## Data collected

- Mass spec (.datraw)
- Sequencing data (fastQ)
- NMR (.fid)
- cell count (images/.xlsx)
- gels (images)
- HPLC (different raw, .xlsx exports)
- notebook (to word)

## Expectations

- Have had RDM before
    - ELN
    - Rules inside bochemsys
    - example of implementations
    - Curation?,  helpdesk?
    - are there **standards** to use?

- New to RDM
    - know the basics and start on the right track
    - What tools to use
    - Information about backups
    - Want to make it understandable for others (in the group)
    - Prevent data theft.

## previous RDM experience

- university rules and LIMS used, the data was saved on university servers automatically
- Large project had some plans, but it was never correctly implemented
- Some rules but not at the group level and no collaboration platforms used.

## other remarks

- progress reports for the whole project -> good way to publicise RDM (and tools). managed by Sabine
 


